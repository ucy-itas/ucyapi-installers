<?php
namespace Composer\Installers;

class UcyApiInstaller extends BaseInstaller
{
    protected $locations = array(
        'route'   => 'routes/{$name}/',
        'service' => 'services/{$name}/',
        'handler' => 'handlers/{$name}/'
    );
}
