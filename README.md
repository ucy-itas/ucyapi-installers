# A [Composer](http://getcomposer.org) Library Installer for UCY Api

This is for PHP package authors to require in their `composer.json`. It will
install their package to the correct location based on the specified package
type.

The goal of Installers is to be a simple package type to install path map.
Users can also customize the install path per package and package authors can
modify the package name upon installing.

Installers isn't intended on replacing all custom installers. If your
package requires special installation handling then by all means, create a
custom installer to handle it.

## Current Supported Package Types

> Stable types are marked as **bold**, this means that installation paths
> for those type will not be changed. Any adjustment for those types would
> require creation of brand new type that will cover required changes.

| Framework    | Types
| ---------    | -----
| Ucyapi       | `ucyapi-route`
|              | `ucyapi-service`
|              | `ucyapi-handler`

## Example `composer.json` File

This is an example for a Ucy api service plugin. The only important parts to set in your
composer.json file are `"type": "ucyapi-service"` which describes what your
package is and `"require": { "ucy/installers": "~1.0" }` which tells composer
to load the custom installers.

```json
{
    "name": "you/ftp",
    "type": "ucyapi-service",
    "require": {
        "composer/installers": "~1.0"
    }
}
```

This would install your package to the `services/ftp/` folder of a CakePHP app
when a user runs `php composer.phar install`.

## Custom Install Paths

If you are consuming a package that uses the `composer/installers` you can
override the install path with the following extra in your `composer.json`:

```json
{
    "extra": {
        "installer-paths": {
            "your/custom/path/{$name}/": ["shama/ftp", "vendor/package"]
        }
    }
}
```

A package type can have a custom installation path with a `type:` prefix.

``` json
{
    "extra": {
        "installer-paths": {
            "your/custom/path/{$name}/": ["type:wordpress-plugin"]
        }
    }
}
```

You can also have the same vendor packages with a custom installation path by
using the `vendor:` prefix.

``` json
{
    "extra": {
        "installer-paths": {
            "your/custom/path/{$name}/": ["vendor:my_organization"]
        }
    }
}
```

These would use your custom path for each of the listed packages. The available
variables to use in your paths are: `{$name}`, `{$vendor}`, `{$type}`.

## Custom Install Names

If you're a package author and need your package to be named differently when
installed consider using the `installer-name` extra.

For example you have a package named `shama/cakephp-ftp` with the type
`cakephp-plugin`. Installing with `composer/installers` would install to the
path `Plugin/CakephpFtp`. Due to the strict naming conventions, you as a
package author actually need the package to be named and installed to
`Plugin/Ftp`. Using the following config within your **package** `composer.json`
will allow this:

```json
{
    "name": "shama/cakephp-ftp",
    "type": "cakephp-plugin",
    "extra": {
        "installer-name": "Ftp"
    }
}
```

Please note the name entered into `installer-name` will be the final and will
not be inflected.